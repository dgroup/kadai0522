
/**
 * 商品情報を管理する商品クラス
 * @author 奥津
 *
 */
public class Item {
	public static int sum;	//入金額の合計値
	String name;			//商品名
	int price;				//商品の値段
	int stock;				//商品の在庫

	/**
	 * コンストラクタでインスタンス作成時に自動入力
	 * @param name 商品名
	 * @param price 値段(>0)
	 * @param stock 在庫(>=0)
	 */
	Item(String name, int price ,int stock){
		this.name = name;
		this.price = price;
		this.stock = stock;
	}

	/**
	 * 引数なしコンストラクタ
	 */
	Item(){
		this.name = null;
		this.price = 0;
		this.stock = 0;
	}
}
