
public class MoneyOutclass{
	//お釣りがどの貨幣が何枚か、を返すメソッド
	static void MoneyOut(){

		//0円だった時の表示
		if(Item.sum == 0){
			System.out.println("入金はありません。");
		}

		else{
			//千円札の枚数
			if(Item.sum >= 1000){
				System.out.println("千円札が" + (Item.sum / 1000) + "枚");
				Item.sum = Item.sum % 1000;
			}

			//500円玉の枚数
			if(Item.sum >= 500){
				System.out.println("500円玉が" + (Item.sum / 500) + "枚");
				Item.sum = Item.sum % 500;
			}

			//100円玉の枚数
			if(Item.sum >= 100){
				System.out.println("100円玉が" + (Item.sum / 100) + "枚");
				Item.sum = Item.sum % 100;
			}

			//50円玉の枚数
			if(Item.sum >= 50){
				System.out.println("50円玉が" + (Item.sum / 50) + "枚");
				Item.sum = Item.sum % 50;
			}

			//10円玉の枚数
			if(Item.sum >= 10){
				System.out.println("10円玉が" + (Item.sum / 10) + "枚");
			}


			//入金制限を解除
			for(int i=Moneyin.count.length-1; i>=0 ;i--){
				Moneyin.count[i] = 0;
			}
			System.out.println("のお釣りが返ってきました。\nまたのお越しをお待ちしております。");
		}
		Item.sum = 0;
	}
}