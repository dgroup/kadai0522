
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	//自販機上部のメソッド
	public String[] Display = {
			"+----------------------------------------------+",
			"|                                              |",
			"|  +---------------------------------------+   |",
			"|  | +-----+   +-----+   +-----+   +-----+ |   |",
			"|  | | コ  |   | オ  |   |     |   | コ  | |   |",
			"|  | | ｜  |   | レ  |   | お  |   | ｜  | |   |",
			"|  | | ラ  |   | ン  |   | 茶  |   | ヒ  | |   |",
			"|  | |     |   | ジ  |   |     |   | ｜  | |   |",
			"|  | +-----+   +-----+   +-----+   +-----+ |   |",
			"|  |  \\150      \\120      \\150      \\120   |   |",
			"|  +---------------------------------------+   |",
			"|                                              |",
			"|      [0]       [1]       [2]       [3]       |",
	};

	//自販機中部のメソッド
	public String[] Display3 ={
			"|                                              |",
			"|                                              |",
			"|        金額                おつり            |",
			"|        +-------+                             |",
	"|        |\\"};

	//自販機下部のメソッド
	public String[] Display2 = {
			"|            〇¬             |",
			"|        +-------+                             |",
			"|                                              |",
			"|    +-----------------------------------+     |",
			"|    |                                   |     |",
			"|    |                                   |     |",
			"|    |                                   |     |",
			"|    |                                   |     |",
			"|    +-----------------------------------+     |",
			"|                                              |",
			"+----------------------------------------------+"
	};
	public static Item[] item = new Item[4];
	public static void main(String[] args) {
		setData();

		String line;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try{

			do {

				/*
				 * 入力処理
				 * 0.画面表示
				 * 1.入力待ち
				 * 2.入力識別(\ -> 入金 , おつり -> 出金 , それ以外 , 商品選択)
				 */

				//0.画面表示
				new Main().show();

				//1.入力待ち
				System.out.println("入力してください ( \\100~\\1000 or おつり or 商品番号(0~3) )");
				line = reader.readLine();
				if( line.equals("") )continue;

				//2.入力文字の識別
				if(line.charAt(0) == '\\'){

					//文字列の先頭が\だった場合、入金処理
					Moneyin.moneyin(line);
				}else if(line.equals("おつり")){

					//文字列が"おつり"だった場合、出金処理
					MoneyOutclass.MoneyOut();
					System.out.println("[ENTER]を押してください");
					reader.readLine();
				}else{

					//それ以外の文字列の場合、商品選択処理
					Shouhinclass.Shouhin(item, line);
					System.out.println("[ENTER]を押してください");
					reader.readLine();
				}

			}while(true);
		}catch (IOException e){

			//入力に例外が発生したならばエラー内容を出力
			System.out.println(e);
		}
	}
	//商品の読み込み
	public static void setData(){
		item[0] = new Item("コーラ",150,10);
		item[1] = new Item("オレンジ",120,10);
		item[2] = new Item("お茶",150,10);
		item[3] = new Item("コーヒー",120,10);
	}

	//画面表示メソッド
	public void show(){

		//自販機上部の表示
		for(int i = 0;i < Display.length; i++){
			System.out.println(Display[i]);
		}

		//ボタンの表示に利用する配列を宣言
		String[] str=new String[4];

		//ボタンの数だけ繰り返し
		for(int i = 0;i<str.length;i++){

			//在庫が無い場合ｳﾘｷﾚ表示を配列に格納
			if(item[i].stock==0){
				str[i]="ｳﾘｷﾚ";
			}else{

				//投入金額によってボタン点灯表示を配列に格納
				if(item[i].price<=Item.sum){
					str[i]="■■";
				}else{
					str[i]="□□";
				}
			}
		}

		//ボタンの表示処理
		System.out.println(		"|     "+str[0]+"      "+str[1]+"      "+str[2]+"      "+str[3]+"       |");

		//自販機中部の表示
		for(int i = 0;i < Display3.length-1; i++){
			System.out.println(Display3[i]);
		}
		System.out.print(Display3[Display3.length-1]);

		//投入金額の表示
		System.out.printf("%6d",Item.sum);

		//自販機下部の表示
		for(int i = 0;i < Display2.length; i++){
			System.out.println(Display2[i]);
		}
	}
}
