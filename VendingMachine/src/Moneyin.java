


public class Moneyin {
	public static int[] count = new int[5]; //入金制限用のクラスフィールド
	public static int[] en = {10,50,100,500,1000}; //入金される金額

	public static boolean checkMoneyIn(int nn){

		int[] countMax = {Integer.MAX_VALUE,Integer.MAX_VALUE,4,3,1}; //各貨幣ごとの入金制限量
		boolean flag = false; //制限されるかどうか

		for(int i=0; i<count.length; i++){
			//入金された貨幣の数をカウント
			if(nn == en[i]){
				count[i] += 1;

				//もし制限量を越したなら
				if(count[i] > countMax[i]){
					count[i]--;

					//入金が拒否される為、戻り値はtrueになる
					flag = true;
				}
			}
		}

		return flag;
	}

	public static void moneyin(String n){
		boolean flag = true;
		String toru = n.replace("\\","");
		try{
			//入力された文字列の先頭の\を取る
			int nn = Integer.parseInt(toru);

			//貨幣判定処理
			switch(nn){
			case 10:
				Item.sum+=10;
				break;
			case 50:
				Item.sum+=50;
				break;
			case 100:
				Item.sum+=100;
				break;
			case 500:
				Item.sum+=500;
				break;
			case 1000:
				Item.sum+=1000;
				break;
			default:
				flag = false;
				System.out.println("10円、50円、100円、500円、1000円のみ入金が可能です。");
				break;
			}

			//戻り値がtrueなら、入金が制限される
			if(checkMoneyIn(nn)){
				System.out.print(nn + ((nn==1000)? "円札": "円玉"));
				System.out.println("の投入枚数が 上限値 になりました。");
				System.out.println("返却いたします。");
				Item.sum -= nn;
			}

		}catch(NumberFormatException e){
			System.out.println("形式が正しくありません。");
		}

	}

}
