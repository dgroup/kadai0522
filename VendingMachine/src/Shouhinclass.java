

public class Shouhinclass {
	public static Item[] Shouhin(Item[] item,String in){

		//文字列を変換して代入するselectを宣言
		int select=0;

		try{

			//文字列を数字に変換
			select=Integer.parseInt(in);

			//ボタンが存在するかの判定
			if(select<item.length && select>=0){

				//在庫があるかの判定
				if(item[select].stock > 0){

					//投入金額が値段より多いかの判定
					if(item[select].price <= Item.sum ){

						//投入金額から値段を引く
						Item.sum -=item[select].price;

						//商品出力
						System.out.println(item[select].name+"が出ました。");

						//在庫を更新
						item=Stockclass.stock(item,select);

						//当たり判定処理
						item=atariclass.atari(item, select);

						//入金制限を解除
						for(int i=0;i<Moneyin.count.length;i++){
							Moneyin.count[i] = 0;
						}

					}else{
						//投入金額が少ない場合
						System.out.println("投入金額が足りません");
					}

				}else{

					//在庫が無い場合の出力
					System.out.println("選択されたボタンの商品は売り切れです");

				}

			}else{
				//選択されたボタンが存在しない場合の処理
				System.out.println("選択されたボタンはありません。");
			}

		//例外処理
		}catch(NumberFormatException e){
			System.out.println("入力が正しくありません");
		}

		//更新したitemを返す。
		return item;
	}
}
