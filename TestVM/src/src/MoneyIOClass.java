package src;


/**
 * 入金計算とお釣り返却計算を行うクラス
 * @author 奥津
 *
 */

public class MoneyIOClass {
	public static int[] EN = {10,50,100,500,1000};	//この自動販売機で扱う金額一覧

	/**
	 * 入金処理を行うメソッド<br>
	 * 入力文字列argsをもとにint型へと変換し、EN配列に該当するならば入金処理を、<br>
	 * 該当しないならばErrMessageを返している
	 * @param args 10円から1000円までの日本円を表す文字列(10円、￥10)
	 * @return ErrMessage 正常時 null , 異常時 エラーメッセージ
	 */

	public String moneyIn(String args) {
		// TODO 自動生成されたメソッド・スタブ
		String ErrMessage = null;
		args = args.replace("\\", "");
		args = args.replace("円", "");
		args = args.replace("￥", "");

		try{
			int num = Integer.parseInt(args);
			int i;
			for(i=0;i<EN.length;i++){
				if(num == EN[i]){
					ItemClass.sum += EN[i];
					break;
				}
			}
			if(i==EN.length){
				ErrMessage = "10円、50円、100円、500円、1000円の入金が可能です";

			}
		}catch (NumberFormatException e){
			ErrMessage = "10円、50円、100円、500円、1000円の入金が可能です";
		}
		return ErrMessage;

	}

	/**
	 * お釣り処理を行うメソッド<br>
	 * 合計金額とEN配列から紙幣、硬貨の金額毎に出力する枚数を求める<br>
	 * @retrun oturi
	 */
	public int[] moneyOut() {
		// TODO 自動生成されたメソッド・スタブ
		int[] oturi;
		oturi = new int[EN.length];
		for(int i=EN.length-1; i>=0; i--){
			if(ItemClass.sum >= EN[i]){
				oturi[i] = ItemClass.sum/EN[i];
				ItemClass.sum %= EN[i];
			}
		}
		ItemClass.sum = 0;
		return oturi;
	}

}
