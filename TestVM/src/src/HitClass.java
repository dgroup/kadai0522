package src;


/**
 * 当たり計算を行うクラス
 * @author 奥津
 *
 */

public class HitClass  {
	private final static int RANDOM = 100;	//当たりの発生確率を決定する定数

	/**
	 * 当たり判定を行うメソッド<br>
	 * ・在庫が0のものは出力しない<br>
	 * ・当たりの場合、購入と異なり合計金額は減らさない<br>
	 *
	 * @param item Item[]型の商品配列
	 * @return message String型の配列、当たり+商品名かはずれかを返す
	 */

	public String hitCheck(ItemClass[] item) {
		// TODO 自動生成されたメソッド・スタブ
		boolean flag = true;
		int r = (int)(Math.random()*100);
		byte[] c = new byte[item.length];
		String message = "";
		if(r<RANDOM){
			do{
				r = (int)(Math.random()*item.length);
				c[r]=1;
				int j;
				for(j=0;j < c.length && c[j] == 1;j++);
				if(j==c.length) flag = false;

			}while(item[r].getStock()==0 && flag);
			if(flag!=false){
				message = new SelectItemClass().Select(item[r]);
				message = "当たりです！　"+message+"";
			}
		}else{
			message = "はずれ";
		}
		return "\n"+message;
	}

}
