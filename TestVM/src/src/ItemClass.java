package src;


public class ItemClass {
	public static int sum;
	private String name;
	private int price;
	private int stock;

	public ItemClass(){

	}
	public ItemClass(String name, int price, int stock){
		this.setName(name);
		this.setPrice(price);
		this.setStock(stock);
	}

	public String getName() {
		// TODO 自動生成されたメソッド・スタブ
		return name;
	}


	public int getPrice() {
		// TODO 自動生成されたメソッド・スタブ
		return price;
	}


	public int getStock() {
		// TODO 自動生成されたメソッド・スタブ
		return stock;
	}


	public void setPrice(int price) throws IllegalArgumentException {
		// TODO 自動生成されたメソッド・スタブ
		if(price <0 || (price%10 != 0)){
			throw new IllegalArgumentException();
		}
		this.price = price;
	}


	public void setStock(int stock) throws IllegalArgumentException {
		// TODO 自動生成されたメソッド・スタブ
		int tmp = this.stock;
		this.stock = stock;
		if(this.stock < 0){
			this.stock = tmp;
			throw new IllegalArgumentException();
		}
	}


	public void setName(String name) {
		// TODO 自動生成されたメソッド・スタブ
		this.name = name;
	}

}
