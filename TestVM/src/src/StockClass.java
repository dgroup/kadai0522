package src;



/**
 * 在庫管理を行うクラス
 * @author 奥津
 *
 */

public class StockClass {
	/**
	 * 在庫を1つ減らす(購入する)メソッド<br>
	 * 購入できない場合、\"売り切れ\"と返す
	 * @param item 対象の商品配列
	 * @param in 商品番号(添え字)
	 * @return str 商品名ないし売り切れと返す
	 */
	public static String stock(ItemClass[] item, int in){
		return stock(item[in]);
	}
	public static String stock(ItemClass item){
		String str;
		try{
			item.setStock( item.getStock()-1 );
			str = item.getName();
		}catch (IllegalArgumentException e){
			str = "売り切れ";
		}
		return str;
	}
}
