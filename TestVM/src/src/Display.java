package src;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;


/**
 * JFrameクラスを用いてラベル、ボタン、テキストフィールドを使い<br>
 * GUIで構成された自動販売機を表示するクラス
 * @author 奥津
 *
 */

public class Display extends JFrame implements ActionListener{
	private final static int WINDOWSIZE_X = 440;
	private final static int WINDOWSIZE_Y = 300;
	Container cp1, cp2;
	JLabel lb1;
	JTextArea txtar1,txtar2;
	public static ItemClass[] item = new ItemClass[4];
	JTextField[] jf = new JTextField[4];
	JTextField MoneyInName,MoneyIn, MoneySum;
	JButton[] btn = new JButton[6];

	JFrame mainFrame;
	/**
	 * 引数なしのコンストラクタ<br>
	 * ボタンやラベルといったGUIに必要なもののレイアウトと<br>
	 * サイズを決定している
	 */
	public Display(){

		setData();

		//新しいディスプレイを作成
		mainFrame = new JFrame("自動販売機");

		//ボタン、ラベルなどを格納するコンテナ(容器)を取得
		cp1 = mainFrame.getContentPane();

		//ボタンやラベルを表示するレイアウト GridBagLayoutを確保
		/*
		 * GridBagLayout
		 *  表のようにボタン、ラベル(文字、画像表示)、テキストフィールド(文字入力欄)
		 *  を並べる為のレイアウトクラス
		 */
		GridBagLayout layout = new GridBagLayout();
		JPanel btn = new JPanel();
		btn.setLayout(layout);


		//ボタンの作成
		this.btn[0] = new JButton("入金");
		this.btn[1] = new JButton("お釣り");

		//商品の数(4)分ボタンを作成、ボタン名は商品名
		for(int i=2;i<6;i++){
			int len = item[i-2].getName().length();
			String str = "";
			for(;len<4;len++){
				str += "　";
			}
			str += item[i-2].getName();
			this.btn[i] = new JButton(str);
		}

		//ボタンを押したときに処理を行うActionListnerに追加
		for(int i=0;i<6;i++){
			this.btn[i].addActionListener(this);
		}

		GridBagConstraints gbc = new GridBagConstraints();


		//ボタン配置 0段目
		gbc.fill = GridBagConstraints.NONE;
		gbc.gridy = 0;
		for(int i=0;i<4;i++){
			JLabel label = new JLabel();
			gbc.gridx = i;
			label.setText(item[i].getName());
			label.setPreferredSize(new Dimension(80,120));
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setBorder(new LineBorder(Color.BLUE, 8, true));
			label.setBackground(Color.WHITE);
			label.setOpaque(true);
			layout.setConstraints(label, gbc);
			btn.add(label);
		}


		gbc.fill = GridBagConstraints.BOTH;
		//ボタン配置 1段目
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		this.jf[0] = new JTextField(item[0].getPrice()+"円");
		this.jf[0].setEditable(false);
		this.jf[0].setHorizontalAlignment(JTextField.CENTER);
		layout.setConstraints(this.jf[0], gbc);
		btn.add(this.jf[0]);
		gbc.gridx = 1;
		this.jf[1] = new JTextField(item[1].getPrice()+"円");
		this.jf[1].setEditable(false);
		this.jf[1].setHorizontalAlignment(JTextField.CENTER);
		layout.setConstraints(this.jf[1], gbc);
		btn.add(this.jf[1]);
		gbc.gridx = 2;
		this.jf[2] = new JTextField(item[2].getPrice()+"円");
		this.jf[2].setEditable(false);
		this.jf[2].setHorizontalAlignment(JTextField.CENTER);
		layout.setConstraints(this.jf[2], gbc);
		btn.add(this.jf[2]);
		gbc.gridx = 3;
		this.jf[3] = new JTextField(item[3].getPrice()+"円");
		this.jf[3].setEditable(false);
		this.jf[3].setHorizontalAlignment(JTextField.CENTER);
		layout.setConstraints(this.jf[3], gbc);
		btn.add(this.jf[3]);

		//ボタン配置 2段目
		gbc.gridwidth = 1;
		gbc.gridy = 2;
		gbc.gridx = 0;
		layout.setConstraints(this.btn[2], gbc);
		btn.add(this.btn[2]);
		gbc.gridx = 1;
		layout.setConstraints(this.btn[3], gbc);
		btn.add(this.btn[3]);
		gbc.gridx = 2;
		layout.setConstraints(this.btn[4], gbc);
		btn.add(this.btn[4]);
		gbc.gridx = 3;
		layout.setConstraints(this.btn[5], gbc);
		btn.add(this.btn[5]);

		/*ボタン配置 3段目*/
		gbc.gridy = 3;
		gbc.gridx = 0;
		this.MoneyInName = new JTextField("投入金額");
		this.MoneyInName.setEditable(false);
		this.MoneyInName.setHorizontalAlignment(JTextField.CENTER);
		layout.setConstraints(this.MoneyInName, gbc);
		btn.add(this.MoneyInName);

		gbc.gridx = 1;
		this.MoneySum = new JTextField("0円");
		this.MoneySum.setHorizontalAlignment(JTextField.RIGHT);
		this.MoneySum.setEditable(false);
		layout.setConstraints(this.MoneySum, gbc);
		btn.add(this.MoneySum);

		gbc.gridx = 2;
		this.MoneyInName = new JTextField("投入口");
		this.MoneyInName.setEditable(false);
		this.MoneyInName.setHorizontalAlignment(JTextField.CENTER);
		layout.setConstraints(this.MoneyInName, gbc);
		btn.add(this.MoneyInName);

		gbc.gridx = 3;
		this.MoneyIn = new JTextField();
		this.MoneyIn.setHorizontalAlignment(JTextField.RIGHT);
		layout.setConstraints(this.MoneyIn, gbc);
		btn.add(this.MoneyIn);



		//ボタン配置 4段目
		txtar1 = new JTextArea(2,32);
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 4;
		gbc.gridheight = 2;
		layout.setConstraints(txtar1, gbc);
		txtar1.setColumns(2);
		txtar1.setEditable(false);
		txtar1.setLineWrap(true);
		btn.add(txtar1);

		//ボタン配置 5段目
		gbc.gridx = 5;
		gbc.gridy = 2;
//		gbc.gridwidth = 2;
		gbc.gridheight = 2;
		layout.setConstraints(this.btn[0],gbc);
		btn.add(this.btn[0]);

		gbc.gridy = 4;
		gbc.gridx = 5;
		layout.setConstraints(this.btn[1],gbc);
		btn.add(this.btn[1]);



		cp1.add(btn, BorderLayout.CENTER);

	}

	/**
	 * 描画を行うメソッド
	 *
	 */
	public void drawDisplay(){
		//終了処理時の動作,xボタンを押したときに終了する
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//画面の大きさを規定、横縦
		mainFrame.setSize(WINDOWSIZE_X, WINDOWSIZE_Y);

		//初期表示位置の決定、nullで中央(デフォルト値)
		mainFrame.setLocationRelativeTo(null);

		//画面の表示非表示
		mainFrame.setVisible(true);
	}

	/**
	 * ボタンが押された際の振る舞いを決定するメソッド<br>
	 * ActionListenerインターフェースからOverrideして実装している
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		String action = e.getActionCommand();

		txtar1.setText("");
		//入金処理
		if(action.equals(this.btn[0].getText())){
			String message = new MoneyIOClass().moneyIn(MoneyIn.getText());
			if(message!=null){
				txtar1.setText(message);
			}
		}

		//出金処理
		if(action.equals(this.btn[1].getText())){
			if(ItemClass.sum!=0){
				int[] rMoney = new MoneyIOClass().moneyOut();
				txtar1.setText(makeString(rMoney));
			}
		}

		for(int i=0;i<4;i++){
			if(action.equals(this.btn[i+2].getText())){
				String m1 = new SelectItemClass().Select(item[i]);
				String m2 = "";
				if(new SelectItemClass().Selectable(item[i]) ){
					m2 = new HitClass().hitCheck(item);
				}
				//txtar1.setText(StockClass.stock(item, i));
				txtar1.setText(m1+m2);
			}
			if( (item[i].getStock()) ==0 ){
				btn[i+2].setText("売り切れ");
			}
		}
		MoneySum.setText(ItemClass.sum+"円");
	}

	/**
	 * おつりの文字列を作成するメソッド<br>
	 * 引数として受け取った配列の中身から1000円札～10円玉までを
	 * 何枚出力するかを決める<br>
	 * MoneyIOClassのEN配列(自販機で扱う金額)と同じ並びでおつりを格納する
	 * int型のoturi配列を受け取る
	 * @param oturi
	 * @return str
	 */
	private String makeString(int[] oturi){
		String str = "";
		boolean flag = true;
		for(int i=oturi.length-1; i>=0;i--){
			if(oturi[i]!=0){
				str += MoneyIOClass.EN[i]+"円"+
						( (MoneyIOClass.EN[i]==1000)? "札":"玉")
						+ oturi[i]+"枚 ";
			}
			if(str.length()>=32 && flag){
				str += "\n";
				flag = false;
			}
		}
		return str+"のお釣りです";
	}

	/**
	 *  商品を表すItem配列を初期化するメソッド
	 */
	private void setData(){
		item[0] = new ItemClass("コーラ",150,10);
		item[1] = new ItemClass("お茶",150,10);
		item[2] = new ItemClass("オレンジ",150,10);
		item[3] = new ItemClass("コーヒー",120,10);
	}

	/**
	 * メインメソッド
	 * @param args 指定なし
	 */
	public static void main(String[] args) {
		Display d = new Display();
		d.drawDisplay();
	}
}
