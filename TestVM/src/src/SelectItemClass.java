package src;

/**
 * 商品選択を行うクラス<br>
 * 購入自体が出来るかを判定する
 * @author 奥津
 *
 */

public class SelectItemClass {

	/**
	 * 引数として与えられたItemクラスのインスタンスを購入するメソッド<br>
	 * @param item 対象となる商品
	 * @return message 購入できた場合、出来なかった場合によるメッセージを返す
	 */

	public String Select(ItemClass item) {
		String message = null;

		try{
			if(item.getStock() > 0){
				if(item.getPrice() <= ItemClass.sum){
					ItemClass.sum -= item.getPrice();

					message = StockClass.stock(item);

					message = "ゴトゴト...ガタン！\t["+item.getName()+"]";
				}else{
					message = "投入金額が不足しています。";
				}
			}else{
				message = "売り切れです";
			}

		}catch (IllegalArgumentException e){
			message = "err";
		}
		// TODO 自動生成されたメソッド・スタブ
		return message;
	}
	/**
	 * 購入自体ができるかを判定するメソッド<br>
	 * @param item 対象となる商品
	 * @return boolean 購入できる場合true 出来ない場合false
	 */
	public boolean Selectable(ItemClass item){
		return (item.getStock()>0)&&(item.getPrice() <= ItemClass.sum);
	}

}
